

/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the Qt Charts module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/
import QtQuick 2.0

// Item 没有 id 属性，内部空间是有id 属性的
Item {
    id: element
    width: 600
    height: 400
    property alias infoDescribeText: infoDescribeText
    property alias infoDescribeStringText: infoDescribeText.text
    property bool sourceLoaded: false // 声明item 的变量，外部可以使用
    property alias currentIndex: rootList.currentIndex // 重命名，把 alias xxx: aaa 把aaa 重命名到 xxx 上
    property alias info: infoText

    ListView {
        id: rootList
        focus: true
        anchors.fill: parent
        snapMode: ListView.SnapOneItem
        highlightRangeMode: ListView.StrictlyEnforceRange
        highlightMoveDuration: 250
        orientation: ListView.Horizontal
        boundsBehavior: Flickable.StopAtBounds

        model: ListModel {
            ListElement {
                component: "View1.qml"
            }
            ListElement {
                component: "View2.qml"
            }
            ListElement {
                component: "View3.qml"
            }
            ListElement {
                component: "View4.qml"
            }
            ListElement {
                component: "View5.qml"
            }
            ListElement {
                component: "View6.qml"
            }
            ListElement {
                component: "View7.qml"
            }
            ListElement {
                component: "View8.qml"
            }
            ListElement {
                component: "View9.qml"
            }
            ListElement {
                component: "View10.qml"
            }
            ListElement {
                component: "View11.qml"
            }
            ListElement {
                component: "View12.qml"
            }
        }

        delegate: Loader {
            width: rootList.width
            height: rootList.height

            source: component
            asynchronous: true

            onLoaded: sourceLoaded = true
        }
    }

    Rectangle {
        id: infoText
        property alias infoDescribeTextt: infoDescribeText
        width: parent.width
        height: 50
        color: "#e94e4e"
        radius: 10
        border.width: 5
        border.color: "#67ea48"
        transformOrigin: Item.Center
        scale: 1
        anchors.verticalCenter: parent.verticalCenter
        opacity: 0.62
        clip: false
        visible: true
        Text {
            id: infoDescribeText
            color: "red"
            anchors.centerIn: parent
            text: "You can navigate between views using swipe or arrow keys10"
        }
    }
}

/*##^##
Designer {
    D{i:16;anchors_height:50;anchors_width:600}
}
##^##*/

