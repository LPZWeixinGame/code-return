#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_QTBase.h"

class QTBase : public QMainWindow
{
    Q_OBJECT

public:
    QTBase(QWidget *parent = nullptr);
    ~QTBase();

private:
    Ui::QTBaseClass ui;
};
